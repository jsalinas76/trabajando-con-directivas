import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { MostrarMensajeComponent } from './mostrar-mensaje/mostrar-mensaje.component';
import { ListadoBasicoComponent } from './listado-basico/listado-basico.component';
import { MiPanelComponent } from './mi-panel/mi-panel.component';
import { ListadoPanelComponent } from './listado-panel/listado-panel.component';

@NgModule({
  declarations: [
    AppComponent,
    MostrarMensajeComponent,
    ListadoBasicoComponent,
    MiPanelComponent,
    ListadoPanelComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
