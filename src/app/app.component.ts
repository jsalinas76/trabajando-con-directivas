import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'trabajando-con-directivas';
  array1 = ['Información básica', 'Información avanzada'];
  array2 = ['Información clásica', 'Información antigua'];

  cadenas = ['Texto a', 'Texto b', 'Texto c'];

}

