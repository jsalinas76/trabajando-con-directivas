import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-mi-panel',
  templateUrl: './mi-panel.component.html',
  styleUrls: ['./mi-panel.component.css'],
})
export class MiPanelComponent implements OnInit {

  @Output() propagar = new EventEmitter<string>();
  @Input() texto: string;
  @Input() claseCss: string = '';

  constructor() {
    this.texto = 'Panel-componente texto';
  }
  ngOnInit(): void {}

  propagarMensaje(parrafo: HTMLElement){
    this.propagar.emit(parrafo.innerHTML);
  }
}
