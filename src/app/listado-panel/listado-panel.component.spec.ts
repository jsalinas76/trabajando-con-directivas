import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListadoPanelComponent } from './listado-panel.component';

describe('ListadoPanelComponent', () => {
  let component: ListadoPanelComponent;
  let fixture: ComponentFixture<ListadoPanelComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListadoPanelComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListadoPanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
