import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-listado-panel',
  templateUrl: './listado-panel.component.html',
  styleUrls: ['./listado-panel.component.css']
})
export class ListadoPanelComponent implements OnInit {

  @Input() claseCss: string = '';
  @Input() textos = new Array<string>();

  mensajeSeleccionado: string = '';

  constructor() { }

  ngOnInit(): void {
  }

  recuperarParrafo(mensaje: string){
    this.mensajeSeleccionado = mensaje;
  }

}
