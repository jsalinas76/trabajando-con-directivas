import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-mostrar-mensaje',
  templateUrl: './mostrar-mensaje.component.html',
  styleUrls: ['./mostrar-mensaje.component.css']
})
export class MostrarMensajeComponent implements OnInit {

  @Input() mensaje: string='';
  visible: boolean = true;
  isBordered = true;
  constructor() { }

  ngOnInit(): void {
  }

  mostrarMensaje(): void{
    this.visible = ! this.visible;
    this.isBordered = ! this.isBordered;
  }

  recuperarNombre(){}

}
