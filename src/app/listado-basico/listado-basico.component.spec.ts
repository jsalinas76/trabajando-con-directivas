import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListadoBasicoComponent } from './listado-basico.component';

describe('ListadoBasicoComponent', () => {
  let component: ListadoBasicoComponent;
  let fixture: ComponentFixture<ListadoBasicoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListadoBasicoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListadoBasicoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
