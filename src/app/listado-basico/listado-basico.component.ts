import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-listado-basico',
  templateUrl: './listado-basico.component.html',
  styleUrls: ['./listado-basico.component.css'],
})
export class ListadoBasicoComponent implements OnInit {

  @Input() cadenas: Array<string>;

  constructor() {
    this.cadenas = new Array<string>();
  }

  ngOnInit(): void {}
}
